<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    //CRUD para modelos
    public function actionCrud(){
        return $this->render("gestion");
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
  
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
 
    
    //Maria consultas
    public function ActionAbout() {
        return $this->render('about');
 
    }  
   public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct(),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
            
        ]);
    }
    
    
    /*CONSULTAS CON DAO*/
public function actionConsulta1(){

$numero = Yii::$app->db
	->createCommand('Select count(distinct edad) from ciclista')
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>'SELECT DISTINCT edad FROM ciclista',

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultado",[

        "resultado"=>$dataProvider,

        "campos"=>['edad'],

        "titulo"=>"Consulta 1 con DAO",

        "enunciado"=>"Listar las edades de los ciclistas (sin repetir)",

        "sql"=>"SELECT DISTINCT edad FROM ciclistas",

]);

}
    
public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct()->where("nomequipo='Artiach'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active record ",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
            
        ]);
    }
       /*CONSULTAS CON DAO*/
public function actionConsulta2(){

$numero = Yii::$app->db
	->createCommand("Select count(distinct edad) from ciclista WHERE nombre ='Artiach'")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo ='Artiach'",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultado",[

        "resultado"=>$dataProvider,

        "campos"=>['edad'],

        "titulo"=>"Consulta 1 con DAO",

        "enunciado"=>"Listar las edades de los ciclistas de Artiach",

        "sql"=>"SELECT DISTINCT edad FROM ciclistas",

]);

}
public function actionConsulta3a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct()->where("nomequipo='Artiach' OR nomequipo='Amore vita'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>" SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita';",
            
        ]);
    }
           /*CONSULTAS CON DAO*/
public function actionConsulta3(){

$numero = Yii::$app->db
	->createCommand(" SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'")
	->queryScalar();


 $dataProvider = new SqlDataProvider([

'sql'=>" SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);
  return $this->render ("resultado",[

        "resultado"=>$dataProvider,

        "campos"=>['edad'],

        "titulo"=>"Consulta 3 con DAO",

        "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",

        "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",

]);

}
public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("dorsal,edad")->distinct()->where("edad < 25 OR edad > 30"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"    SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30;",
            
        ]);
    }
               /*CONSULTAS CON DAO*/
public function actionConsulta4(){

$numero = Yii::$app->db
	->createCommand("SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30")
	->queryScalar();


 $dataProvider = new SqlDataProvider([

'sql'=>"SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);
  return $this->render ("resultado",[

        "resultado"=>$dataProvider,

        "campos"=>['dorsal'],

        "titulo"=>"Consulta 4 con DAO",

        "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",

        "sql"=>"SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30",

]);

}
public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("dorsal,edad")->distinct()->where("edad BETWEEN 28 AND 32 AND nomequipo= 'Banesto'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>" SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo= 'Banesto' ;",
            
        ]);
    }
                   /*CONSULTAS CON DAO*/
public function actionConsulta5(){

$numero = Yii::$app->db
	->createCommand(" SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo= 'Banesto' ;")
	->queryScalar();


 $dataProvider = new SqlDataProvider([

'sql'=>"SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);
  return $this->render ("resultado",[

        "resultado"=>$dataProvider,

        "campos"=>['dorsal'],

        "titulo"=>"Consulta 5 con DAO",

        "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",

        "sql"=>"SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad < 25 OR edad > 30",

]);

}
public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nombre")->distinct()->where("LENGTH(nombre) > 8"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE LENGTH(nombre) > 8;",
            
        ]);
    }
public function actionConsulta6(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM  ciclista WHERE CHAR_LENGTH(nombre)>8")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT nombre FROM  ciclista WHERE CHAR_LENGTH(nombre)>8",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
        return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM  ciclista WHERE CHAR_LENGTH(nombre)>8)",
        ]);
       
    }


public function actionConsulta7a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("UPPER(nombre) nombre , dorsal ")->distinct(),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>" lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT upper (nombre) AS Nombre_Mayusculas, dorsal FROM ciclista; ",
            
        ]);
    }

public function actionConsulta7(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) ciclista")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT upper (nombre)nombre ,dorsal FROM ciclista",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
        return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT upper (nombre)nombre , dorsal FROM ciclista",
        ]);
       
    }
    public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find() ->select("dorsal ")->distinct()->where("código='MGE'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"  Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT FROM lleva where código='MGE'; ",
            
        ]);
    }
    public function actionConsulta8(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT DISTINCT dorsal FROM lleva where código='MGE'")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM lleva where código='MGE'",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
        return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>" Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva where código='MGE';",
        ]);
       
    }
       public function actionConsulta9a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("nompuerto ")->distinct()->where("altura > 1500"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"  Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto; ",
            
        ]);
    }
        public function actionConsulta9(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE altura > 1500")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
        return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"  Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500",
        ]);
       
    }
           public function actionConsulta10a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("dorsal ")->distinct()->where("dorsal=dorsal AND altura BETWEEN 1800 AND 3000 OR pendiente > 8"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 OR pendiente >8; ",
            
        ]);
    }
       public function actionConsulta10(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE dorsal=dorsal AND altura BETWEEN 1800 AND 3000 OR pendiente >8")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 OR pendiente >8",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 OR pendiente >8",
        ]);
       
    }
       public function actionConsulta11a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("dorsal ")->distinct()->where("dorsal=dorsal AND altura BETWEEN 1800 AND 3000 AND pendiente > 8"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 AND pendiente >8; ",
            
        ]);
    }
       public function actionConsulta11(){
       
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE dorsal=dorsal AND altura BETWEEN 1800 AND 3000 AND pendiente >8")
                ->queryScalar();
       
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 AND pendiente >8",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
       
return $this->render('resultado', [
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto where dorsal=dorsal AND altura BETWEEN 1800 AND 3000 AND pendiente >8",
        ]);
       
    }

}